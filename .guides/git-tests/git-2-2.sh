#!/bin/bash
QCOUNT=3

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Create a file named remote-file.txt in the file tree" "remote-file.txt" "ls /home/codio/workspace"
				;;
			2 )
				expect "Add a new remote called github that points to the same URL as the origin repo" "github[[:space:]]*" "git remote -v"
				;;
			3 )
				expect "Stage and commit the changes adding the requested message" "nothing to commit" "git status"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command