#!/bin/bash
QCOUNT=3

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Create a file named to-remote-repo.txt in the file tree" "to-remote-repo.txt" "ls /home/codio/workspace"
				;;
			2 )
				expect "Add a new remote called new-origin that points to the same URL as the origin repo" "new-origin[[:space:]]*" "git remote -v"
				;;
			3 )
				expect "Commit the changes adding the requested message" "Pushing to new-origin repo" "git log"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command