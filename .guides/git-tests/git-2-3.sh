#!/bin/bash
QCOUNT=2

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
			1 )
				expect "Commit the changes in the remote repo using the requested message" "Updated remote-file.txt" "git log"
				;;
			2 )
				expect "Pull the changes to your local repo" "Updated remote-file.txt" "git log"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command