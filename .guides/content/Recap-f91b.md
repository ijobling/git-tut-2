You are now familiar with the basics of the source control management system called _Git_. 

In the previous unit, you learned how to: 

- _Stage_ files or directories that were added or edited inside the working directory hierarchy
- _Commit_ the changes to the local repository, which is in fact, stored in the working directory as a hidden folder named `.git`
- Get the _status_ of your files and directories (new file, edited file, non-staged, committed, etc.)
- _Reset_ the state of your working directory by going back 1 or more commits 

