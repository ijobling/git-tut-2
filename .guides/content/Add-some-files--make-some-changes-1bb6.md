So to feel that you did something with this project, make some changes to your project. 

Add a new file and edit the contents of any of the files.

Once you’re done, you can stage and commit with 

```bash
git add -A (stage all tracked and untracked files)
git commit -m 'a few of my own changes' (to commit)
```

Assuming you added a file named `new-file.txt` in the file tree, you should see something like this (some info may vary): 

```bash
[master (root-commit) 0b6df30] a few of my own changes
 4 files changed, 64 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 .settings
 create mode 100644 mary.txt
 create mode 100644 new-file.txt
```

---
Complete a challenge about remote repositories and GitHub in the next section.