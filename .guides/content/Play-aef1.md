Now make some more local changes using the same sequence we did earlier, so add files, modify content etc.

```
git add -A  (to add all changes to the staging area)
git commit -m 'commit message' (to commit)
```

Once you’ve done a commit or two, push everything again using just 

```
git push
```

and refresh your GitHub repo page to see the changes.

Note that `git push` is all it takes to do your pushes as long as you used the `-u` switch in the `git push origin master -u ….` command.

Keep playing around with things until you are completely happy with the process. Don’t worry about messing things up. Just get really comfortable and allow yourself to make mistakes. If you want to go back to the beginning, you can do the following

- `git reset --hard HEAD` to get your local back to the initial state for this module
- delete your GitHub repo in GitHub repeat the setup process
