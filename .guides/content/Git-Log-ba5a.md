The pull we did just created a new local commit. If you type

```
git log
```

you will see a history of all commits in the repo, including the one that was pulled in from the remote repo. We’ll talk more about `git log` in the next unit.

---
Time for a set of code challenges about remote repos.

Go to the next section.