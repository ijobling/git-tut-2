As a final thing, it is really easy to create a project from someone else’s GitHub repo. So you’ve got something nice and simple to play with, go to this URL:

```
https://github.com/fmay/git-video-overview
```

This is a very simple project that we used to put together the Video Overview. 

To begin editing this project in a new Codio instance, follow these instructions:

1. Open a new browser tab or window and go to _codio.com/home_
1. From the Codio projects screen create a brand new Codio project by pressing the _New Project_ button in the top-right part
2. On the next screen, below the _1) Select your starting point_ boxes, click on the _Click here to import, or select an alternative software configuration_ link to display more import options
3. Select the _Import_ tab and paste the following URL in the URL input field:

```
https://github.com/fmay/git-video-overview.git
```

4. Give the project a name, add a short description and click the "Create" button

Now you can see the code in the Codio IDE and can start playing with it.

You will be able to edit the code but you can’t push modifications back to GitHub because you won’t have permissions.