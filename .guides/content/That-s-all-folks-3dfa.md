There’s a lot more to working with remote repos, but we’ll leave it there for the time being and come back to look at more advanced examples another time.

In the next unit, you will learn how to isolate each bug or feature you’re working on using _git branches_.