Now, we‘re ready do push our local repo to the remote Github repo. 

If you type 

```
git log
```

you can see that there is a commit in our repo all ready to push.

So, let’s push:

```
git push -u origin master
```

- `-u` tells Git that the named remote (the next parameter 'origin') is the ‘upstream’ remote and so after this initial command, just using `git push` on its own is all that is needed, without having to type the full command above
- `origin` is the name of the remote that you want to push to and was defined in the previous section with `git remote add origin remote-url`
- `master` is the name of the branch that you want to push (branches will be discussed in the next unit)

You will be prompted to enter your GitHub user name and password. Once you have done this, your local repo will be pushed to the remote repo.

Back on your GitHub page, refresh the repo you created there and you’ll see your files. You can drill down into the files and explore. 

![](.guides/img/git-explore-2.png)

That’s pretty cool. You now have a remote backup of your repo and, if it’s a public GitHub repo, others can access your code, but that’s for another time.

