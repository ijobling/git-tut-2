Now we’ll do things the other way round and actually make some changes on the remote GitHub repo before pulling them into our local repo.

## Commit Changes
First of all, make sure you have committed your local changes and pushed them exactly as discussed in the previous section.

## Edit a file on GitHub
Next, go to your GitHub repo and edit a file there by clicking on a file and then pressing the edit button. Having made your edits, enter a short commit message and press the 'Commit Changes' button.

## Git Status
Now, back in your local repo, you can check `git status`, but this does not really help you. Your local repo does not know about remote changes.

## Pull in remote changes
So, let’s pull in remote changes now using

```
git pull
```

Now, any remote changes you made will appear in your local project.