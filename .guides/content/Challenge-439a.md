Verify that there are no local changes to commit by doing a `git status`. If there are, commit them before going on with this challenge.

{Check It!|assessment}(test-1198273632)

Keep going with the second part of this set of challenges in the next section.

|||guidance

### Correct answers:

1. Create a file `remote-file.txt` in the workspace folder
2. `git remote add github https://github.com/your-gituhub-user-name/your-remote.git` (must be same URL as `origin` repo)
3. `git add remote-file.txt`, `git commit -m "Pushing remote-file.txt"`
4. `git push github master`

|||