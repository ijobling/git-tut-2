{Check It!|assessment}(test-3067571133)


|||guidance

### Correct answers:

1. Create the file `to-remote-repo.txt` in the file tree
2. `git remote add new-origin https://github.com/your-gituhub-user-name/your-remote.git` (must be same URL as `origin` repo)
3. `git add -A`
4. `git commit -m "Pushing to new-origin repo"`
5. `git push new-origin master`

|||