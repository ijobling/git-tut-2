Make sure you have your Codio project in front of you. 

The first thing we need to do is to tell Git where the remote repo is located. You should already have the remote repo URL copied onto the clipboard (done in the last section) and you’ll be adding this as the last parameter you will enter into the Terminal as shown below (you may need to scroll to the right to see the whole line).

```
git remote add origin https://github.com/your-gituhub-user-name/your-remote.git
```

Let’s analyse this command

- `git remote add` tells Git to associate our project with a new remote repository
- `origin` is the name you decided to give to this remote repo. `origin` is the name most people use for the primary remote repo
- `https://github.com/your-gituhub-user-name/your-remote.git` is the URL of that repo. You get this from within GitHub as shown in the video (the bit you copied to the clipboard).

You can check the remote details at any time using

```
git remote -v
```