This challenge requires you to complete challenge 5.2 in the previous section.

{Check It!|assessment}(test-2536823966)

|||guidance

### Correct answers:

1. Edit the `remote-file.txt` file in the online GitHub repo
2. After editing, click the _commit_ button and add the message: “Updated remote-file.txt”
3. `git pull github master` or `git pull` 

|||