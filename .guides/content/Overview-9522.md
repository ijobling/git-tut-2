## Remote Repositories

This unit comes with some files already created so you are ready to get down to business immediately.

## GitHub
GitHub is a platform that stores and manages remote repositories. 

As we will soon see, you can ‘push’ your local Git repo onto GitHub whenever you like. It is amazingly fast and there are many benefits to doing this, not least of which is that you have a remote backup of your entire project history, including commit snapshots. 

Being able to collaborate with other developers on the exact same code base is also a pretty big deal.

## Create a GitHub Account and an empty repository
The first thing you should do is to create a GitHub account. Watch the video below to see how to do this and also set up a new, empty repo ready to push your code into.

![640x480](http://www.youtube.com/watch?v=phzfEqEIt3g&feature=youtu.be)

Make sure you have the GitHub repo URL copied to the clipboard, which was the last step in the above video. It should look something like this

```
https://github.com/fmayx/mynewrepo.git
```

You should also have ready the GitHub user name and password that used for your GitHub account signup.
